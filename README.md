# dotscripts #

dotscripts is a collection of GraphViz, especially gvpr-scripts to extract
information out of DAGs. It's mainly used by me for Guix to get condensed
information out of huge package graphs.

## License ##

dotscripts are copyrighted 2018 by Björn Höfling.
dotscripts are licensed under the GNU GPL. Either version 3 or any later
version, at your option.
