/*

dotscripts --- Scripts for GraphViz

Copyright © 2018 Björn Höfling <bjoern.hoefling@bjoernhoefling.de>

This file is part of dotscripts.

dotscripts is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

dotscripts is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dotscripts.  If not, see <http://www.gnu.org/licenses/>.
*/


/******************************************************************************* 
 Displays the rank of each node.
 This is a simple gvpr script.
 Usage: gvpr -f rank.g <mygraph.dot>

*******************************************************************************/

BEG_G {
    $tvtype = TV_dfs;
}

N [] {
    string namex = $.name;
    int id = $.indegree;
    int od = $.outdegree;
    int dd = $.degree;
    string ll = $.label;
    printf("NODE: %s[label: %s], indegree: %i, outdegree: %i, degree: %i\n", name, label, indegree, outdegree, degree);
}
