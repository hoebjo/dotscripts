/*

dotscripts --- Scripts for GraphViz

Copyright © 2018 Björn Höfling <bjoern.hoefling@bjoernhoefling.de>

This file is part of dotscripts.

dotscripts is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

dotscripts is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dotscripts.  If not, see <http://www.gnu.org/licenses/>.
*/


/******************************************************************************* 

 Some statistics for graphs.

 gvpr -f stats.g mygraph.dot

 If you want the leaf nodes to be printed, call it with "-leaf" argument:

 gvpr -f stats.g -a "-leaf" mygraph.dot

*******************************************************************************/
BEGIN {
    int printLeafs;
    
    if(ARGC>0 && ARGV[0]=="-leaf") {
        printLeafs = 1;
    } else {
        printLeafs = 0;
    }
}

BEG_G {
    $tvtype = TV_dfs;
    int nodes = 0;
    int edges = 0;
    int idegree[node_t];
    int odegree[node_t];

    int maxidegree=0;
    int maxodegree=0;
    node_t maxin;
    node_t maxout;
    
}

N [] {
    nodes++;
    idegree[$]=$.indegree;
    odegree[$]=$.outdegree;
    if($.indegree>maxidegree) {
        maxidegree=$.indegree;
        maxin=$;
    }


    if($.outdegree>maxodegree) {
        maxodegree=$.outdegree;
        maxout=$;
    }
}

E[] {
    edges++;
}

END_G {
    printf("Nodes: %d\n", nodes);
    printf("Edges: %d\n", edges);
    printf("Maximum indegree : %d\n", maxidegree);
    printf("Maximum outdegree: %d\n", maxout.degree);

    node_t currNode;
    printf("\nNode(s) with maximum outdegree:\n");
    for(odegree[currNode]) {
        if(currNode.outdegree==maxodegree) {
            printf("%s[%s]\n", currNode.name, currNode.label);
        }
    }

    printf("\nNode(s) with maximum indegree:\n");
    for(idegree[currNode]) {
        if(currNode.indegree==maxidegree) {
            printf("%s[%s]\n", currNode.name, currNode.label);
        }
    }

    //Calculate number of leafs:
    int numLeafs=0;
    for(odegree[currNode]) {
        if(odegree[currNode]==0) {
            numLeafs++;
        }
    }

    printf("\nLeaf nodes: %d\n", numLeafs);
   
    if(printLeafs) {
        for(odegree[currNode]) {
            if(odegree[currNode]==0) {
                printf("Leaf node: %s[label=%s]\n", currNode.name, currNode.label);
            }
        }
    }
}

