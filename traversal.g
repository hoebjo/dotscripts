/*

dotscripts --- Scripts for GraphViz

Copyright © 2018 Björn Höfling <bjoern.hoefling@bjoernhoefling.de>

This file is part of dotscripts.

dotscripts is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

dotscripts is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dotscripts.  If not, see <http://www.gnu.org/licenses/>.
*/


/******************************************************************************* 
Usage: gvpr -f traversal.g <my-graph.dot>

Example that visualizes the traversal of nodes/edges.
Prints the name of each each node and each edge when visited.

You can play with the start $tvtype to change the traversal order
(BFS, DFS, ...) or change the start note with $tvnext.
*******************************************************************************/

BEG_G {
    // Set the starting node. 
    //$tvnext = node($,"A4");
    $tvtype = TV_bfs;
    // Try also:
    //TV_bfs; Breath first search
    //TV_rev; Reverse order
    //TV_fwd; Forward order
    // TV_prepostdfs; DFS, visiting first the node, then the edges, finally visiting the node again.
}

E{
    printf("E: %s\n", $.name);
}

N {
    printf("N: %s\n", $.name);
}
